import LeNet5_inference
import LeNet5_train
import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
import os

# CKPT_PATH="./ckpt/"
BATCH_SIZE=10
LEARNING_RATE_BASE=0.8
TRAINING_STEPS = 30000
MOVING_AVERAGE_DECAY = 0.99
MODEL_SAVE_PATH="./model/"
TRAIN_STEPS = 10000

def evaluate(mnist):
    with tf.name_scope("input"):
        x = tf.placeholder(tf.float32,[None, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_CHANNEL],
                           name="input-x")
        y_label = tf.placeholder(tf.float32, [None, LeNet5_inference.OUTPUT_NODE], name="input-label")
        y = LeNet5_inference.inference(x)
        validate_feed = {x: mnist.test.images, y_label: mnist.test.labels}
        yargmax=tf.argmax(y, 1, name="yargmax")
        y_label_argmax=tf.argmax(y_label, 1, name="y_label_argmax")

    with tf.name_scope("accuracy"):
        correct_prediction = tf.cast(tf.equal(tf.argmax(y, 1), tf.argmax(y_label, 1)), tf.float32)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # variable_averages = tf.train.ExponentialMovingAverage(LeNet5_train.MOVING_AVERAGE_DECAY)
    # variables_to_restore = variable_averages.variables_to_restore()
    saver = tf.train.import_meta_graph(LeNet5_train.MODEL_SAVE_PATH+".meta")

    # saver = tf.train.Saver(max_to_keep=3)
    # global_step = tf.Variable(0, trainable=False)

    with tf.Session() as sess:
        ckpt = tf.train.get_checkpoint_state(LeNet5_train.MODEL_SAVE_PATH)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]

            init_op = tf.global_variables_initializer()
            sess.run(init_op)  # init

            xs, ys = mnist.test.next_batch(BATCH_SIZE)
            xs_reshaped = np.reshape(xs, (BATCH_SIZE, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_CHANNEL))
            # print(xs)
            validate_feed={x:xs_reshaped,y_label:ys}

            y_label_,y_,accuracy1=sess.run([y_label,y,accuracy],feed_dict=validate_feed)
            o_yargmax,o_y_label_argmax=sess.run([yargmax,y_label_argmax],feed_dict=validate_feed)

            print(o_yargmax)
            print(o_y_label_argmax)
            # print(y_label_)
            print(y_)
            print(accuracy1)

def main(argv=None):
    mnist = input_data.read_data_sets("../minst/datasets/MNIST_data", one_hot=True)
    evaluate(mnist)

if __name__ == '__main__':
    tf.app.run()