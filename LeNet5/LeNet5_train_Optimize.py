import LeNet5_inference
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import os
CKPT_PATH="./ckpt/"
BATCH_SIZE=50
LEARNING_RATE_BASE=0.8
TRAINING_STEPS = 30000
MOVING_AVERAGE_DECAY = 0.99

def train(input_data):
    with tf.name_scope("input"):
        x=tf.placeholder(tf.float32, [BATCH_SIZE, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_CHANNEL], NAME="X_INPUT")
        y_label=tf.placeholder(tf.float32, [BATCH_SIZE, LeNet5_inference.OUTPUT_NODE], name="Y_INPUT")
        y=LeNet5_inference.inference(x)
        global_step = tf.Variable()
    with tf.name_scope("accuracy"):
        correct_prediction=tf.cast(tf.equal(tf.argmax(y_label,1),tf.argmax(y,1)),tf.float32)
        accuracy=tf.reduce_mean(correct_prediction)

    with tf.name_scope("loss"):
        cross_entropy=tf.nn.softmax_cross_entropy_with_logits(labels=y_label,logits=y)
        cross_entropy_mean=tf.reduce_mean(cross_entropy)

    variable_averages = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, global_step)
    variables_averages_op = variable_averages.apply(tf.trainable_variables())

    with tf.name_scope("train_step"):

        # train_step=tf.train..GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)
        learning_rate = tf.train.exponential_decay(LEARNING_RATE_BASE,global_step,input_data.train.num_examples / BATCH_SIZE, LEARNING_RATE_DECAY,staircase=True)
        train_step=tf.train.GradientDescentOptimizer(learning_rate).minimize(cross_entropy,global_step)
    saver=tf.train.Saver()

    with tf.control_dependencies([train_step, variables_averages_op]):
        train_op = tf.no_op(name='train')

    """ read ckpt """
    # with tf.Session()as sess:
    #     ckpt=tf.train.get_checkpoint_state(CKPT_PATH)
    #     if ckpt and ckpt.model_checkpoint_path:
    #         saver.restore(sess,ckpt.model_checkpoint_path)
    #         global_step=ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        for i in range(TRAINING_STEPS):
            xs,ys=input_data.train.next_batch(BATCH_SIZE)
            # _,loss_value,step=sess.run([train_op, loss, global_step], feed_dict={x: xs, y_: ys})