from tensorflow.examples.tutorials.mnist import input_data
import os
import numpy as np
import tensorflow as tf

MODEL_SAVE_PATH = r"Model"
MODEL_NAME = "model.ckpt"
layer1_path="data/layer1/"
layer2_path="data/layer2/"
layer3_path="data/layer3/"
layer4_path="data/layer4/"
layer5_path="data/layer5/"
layer6_path="data/layer6/"
layer7_path="data/layer7/"
other_path="data/other/"

def load_data():
    BATCH=10000
    mnist = input_data.read_data_sets(r"MNIST_data", one_hot=True)
    train_data, train_label = mnist.train.images, mnist.train.labels
    train_data = np.reshape(train_data, (train_data.shape[0], 28, 28, 1))
    # test_data,test_label=mnist.test.next_batch(BATCH)
    test_data, test_label = mnist.test.images, mnist.test.labels
    test_data = np.reshape(test_data, (BATCH, 28, 28, 1))
#test_data.shape[0]

    train_image_num = len(train_data)
    train_image_index = np.arange(train_image_num)
    np.random.shuffle(train_image_index)
    train_data = train_data[train_image_index]
    train_label = train_label[train_image_index]


    # test_image_num = len(test_data)
    # test_image_index = np.arange(test_image_num)
    # np.random.shuffle(test_image_index)
    # test_data = test_data[test_image_index]
    # test_label = test_label[test_image_index]
    return test_data, test_label, train_data, train_label


def inference(input_tensor, train=False, regularizer=None):

    with tf.variable_scope('layer1-conv1'):
        conv1_weights = tf.Variable(initial_value=tf.truncated_normal(shape=[5, 5, 1, 6], stddev=0.1), name='weight')
        conv1_biases = tf.Variable(initial_value=tf.constant(shape=[6], value=0.0), name='bias')
        conv1 = tf.nn.conv2d(input_tensor, conv1_weights, strides=[1, 1, 1, 1], padding='SAME')
        layer1 = tf.nn.relu(tf.add(conv1, conv1_biases), name="layer1")


    with tf.name_scope('layer2-pool1'):
        pool1 = tf.nn.max_pool(layer1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name="pool1")


    with tf.variable_scope('layer3-conv2'):
        conv2_weights = tf.Variable(initial_value=tf.truncated_normal(shape=[5, 5, 6, 16], stddev=0.1), name='weight')
        conv2_biases = tf.Variable(initial_value=tf.constant(shape=[16], value=0.0), name='bias')
        conv2 = tf.nn.conv2d(pool1, conv2_weights, strides=[1, 1, 1, 1], padding='VALID')
        layer3 = tf.nn.relu(tf.add(conv2, conv2_biases), name="conv2")


    with tf.variable_scope('layer4-pool2'):
        pool2 = tf.nn.max_pool(layer3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name="pool2")

    #5
    with tf.variable_scope('layer5-fc1'):
        fc1_weights = tf.Variable(initial_value=tf.truncated_normal(shape=[5*5*16, 120], stddev=0.1), name='weight')
        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(fc1_weights))
        fc1_biases = tf.Variable(initial_value=tf.constant(shape=[120], value=0.1), name='bias')
        flat_pool2 = tf.reshape(pool2, shape=[-1, 5*5*16])
        fc1 = tf.nn.relu(tf.matmul(flat_pool2, fc1_weights) + fc1_biases, name="fc1")
        # tf.add_to_collection("fc1", fc1)
        # if train==True:
        #     fc1 = tf.nn.dropout(fc1, 0.5)

    # l6
    with tf.variable_scope('layer6-fc2'):
        fc2_weights = tf.Variable(initial_value=tf.truncated_normal(shape=[120, 84], stddev=0.1), name='weight')
        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(fc2_weights))
        fc2_biases = tf.Variable(initial_value=tf.constant(shape=[84], value=0.1), name='bias')
        fc2 = tf.nn.relu(tf.matmul(fc1, fc2_weights) + fc2_biases, name="fc2")
        # tf.add_to_collection("fc2", fc2)
        # if train==True:
        #     fc2 = tf.nn.dropout(fc2, 0.5)

    # l7
    with tf.variable_scope('layer7-fc3'):
        fc3_weights = tf.Variable(initial_value=tf.truncated_normal(shape=[84, 10], stddev=0.1), name='weight')
        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(fc3_weights))
        fc3_biases = tf.Variable(initial_value=tf.constant(shape=[10], value=0.1), name='bias')
        logit = tf.add(tf.matmul(fc2, fc3_weights), fc3_biases, name="output")
        # tf.add_to_collection("fc3", logit)
    return logit


def main(test_data, test_label, train_data, train_label, command=None):

    if command is None:
        print("Please input train or test!")
        return
    elif command == "test":
        # x =tf.placeholder()
        # y = inference(x, False, None)
        # correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
        # accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")
        # sess = tf.Session()
        # saver = tf.train.Saver()
        # saver.restore(sess, r"Model/model.ckpt")

        with tf.Session() as sess:
            ckpt = tf.train.get_checkpoint_state(MODEL_SAVE_PATH)
            if ckpt and ckpt.model_checkpoint_path:
                # sess = tf.Session()
                saver = tf.train.import_meta_graph(r'Model/model.ckpt.meta')
                saver.restore(sess, r"Model/model.ckpt")
                graph = tf.get_default_graph()
                """print all tensor"""
                # tensor_name_list = [tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
                # print(tensor_name_list)
                input_x = graph.get_tensor_by_name("x:0")
                label_y_ = graph.get_tensor_by_name("y_:0")
                """output l1"""
                layer1=graph.get_operation_by_name("layer1-conv1/layer1").outputs[0]
                layer1_op=sess.run(layer1,feed_dict={input_x: test_data, label_y_: test_label})
                for i in range(len(layer1_op)):
                    with open(layer1_path+str(i)+".txt","w")as file:
                        for j in range(len(layer1_op[i])):
                            for k in range(len(layer1_op[i][j])):
                                for m in range(len(layer1_op[i][j][k])):
                                    file.write(str(layer1_op[i][j][k][m])+" ")
                                file.write("\n")
                        file.write("\n")

                """op l2"""
                # layer2=graph.get_operation_by_name("layer2-pool1/pool1").outputs[0]
                # layer2_op=sess.run(layer2,feed_dict={input_x: test_data, label_y_: test_label})
                # print(layer2_op.shape)
                # for i in range(len(layer2_op)):
                #     with open(layer2_path+str(i)+".txt","w")as file:
                #         for j in range(len(layer2_op[i])):
                #             for k in range(len(layer2_op[i][j])):
                #                 for m in range(len(layer2_op[i][j][k])):
                #                     file.write(str(layer2_op[i][j][k][m])+" ")
                #                 file.write("\n")
                #         file.write("\n")

                """op l3"""
                # layer3=graph.get_operation_by_name("layer3-conv2/conv2").outputs[0]
                # layer3_op=sess.run(layer3,feed_dict={input_x: test_data, label_y_: test_label})
                # print(layer3_op.shape)
                # for i in range(len(layer3_op)):
                #     with open(layer3_path+str(i)+".txt","w")as file:
                #         for j in range(len(layer3_op[i])):
                #             for k in range(len(layer3_op[i][j])):
                #                 for m in range(len(layer3_op[i][j][k])):
                #                     file.write(str(layer3_op[i][j][k][m])+" ")
                #                 file.write("\n")
                #         file.write("\n")
                """op l4"""
                # layer4=graph.get_operation_by_name("layer4-pool2/pool2").outputs[0]
                # layer4_op=sess.run(layer4,feed_dict={input_x: test_data, label_y_: test_label})
                # print(layer4_op.shape)
                # for i in range(len(layer4_op)):
                #     with open(layer4_path+str(i)+".txt","w")as file:
                #         for j in range(len(layer4_op[i])):
                #             for k in range(len(layer4_op[i][j])):
                #                 for m in range(len(layer4_op[i][j][k])):
                #                     file.write(str(layer4_op[i][j][k][m])+" ")
                #                 file.write("\n")
                #         file.write("\n")

                """op l5"""
                # layer5=graph.get_operation_by_name("layer5-fc1/fc1").outputs[0]
                # layer5_op=sess.run(layer5,feed_dict={input_x: test_data, label_y_: test_label})
                # print(layer5_op.shape)
                # for i in range(len(layer5_op)):
                #     with open(layer5_path+str(i)+".txt","w")as file:
                #         for j in range(len(layer5_op[i])):
                #             file.write(str(layer5_op[i][j])+"\n")

                """op l6"""
                # layer6=graph.get_operation_by_name("layer6-fc2/fc2").outputs[0]
                # layer6_op=sess.run(layer6,feed_dict={input_x: test_data, label_y_: test_label})
                # print(layer6_op.shape)
                # for i in range(len(layer6_op)):
                #     with open(layer6_path+str(i)+".txt","w")as file:
                #         for j in range(len(layer6_op[i])):
                #             file.write(str(layer6_op[i][j])+"\n")

                """op l7"""
                # layer7=graph.get_operation_by_name("layer7-fc3/output").outputs[0]
                # layer7_op=sess.run(layer7,feed_dict={input_x: test_data, label_y_: test_label})
                # print(layer7_op.shape)
                # for i in range(len(layer7_op)):
                #     with open(layer7_path+str(i)+".txt","w")as file:
                #         for j in range(len(layer7_op[i])):
                #             file.write(str(layer7_op[i][j])+"\n")
                #
                # y=graph.get_operation_by_name("layer7-fc3/output").outputs[0]
                # y_argmax=tf.argmax(y,1)
                # op_argmax_output=sess.run(y,feed_dict={input_x: test_data, label_y_: test_label})
                # """op_arg_max"""
                #
                #
                # correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(label_y_, 1))
                # accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")
                # accuracy_op=sess.run(accuracy,feed_dict={input_x: test_data, label_y_: test_label})

                # for i in op_argmax_output:
                #     print(i)
                # accuracy = graph.get_tensor_by_name("accuracy:0")
                # layer1_weight = graph.get_tensor_by_name("layer1-conv1/weight:0")
                # layer1_weight, acc = sess.run([layer1_weight, accuracy], feed_dict={input_x: test_data, label_y_: test_label})

                # print(layer1_op.shape)
                # print("testing accuracy:", accuracy_op)

                return

    elif command == "train":
        global_step = tf.Variable(0, trainable=False)
        x = tf.placeholder(tf.float32, [None, 28, 28, 1], name='x')
        y_ = tf.placeholder(tf.float32, [None, 10], name='y_')
        regularizer = tf.contrib.layers.l2_regularizer(0.001)
        y = inference(x, True, regularizer)
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y, labels=tf.argmax(y_, 1))
        cross_entropy_mean = tf.reduce_mean(cross_entropy)
        loss = cross_entropy_mean + tf.add_n(tf.get_collection('losses'))
        train_op = tf.train.AdamOptimizer(0.001).minimize(loss,global_step=global_step)
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")

        saver = tf.train.Saver()
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

            def get_batch(data, label, batch_size):
                for start_index in range(0, len(data) - batch_size + 1, batch_size):
                    slice_index = slice(start_index, start_index + batch_size)
                    yield data[slice_index], label[slice_index]

            #
            #
            train_num = 5
            batchsize = 64
            for i in range(train_num):
                train_loss, train_acc, batch_num = 0, 0, 0
                for train_data_batch, train_label_batch in get_batch(train_data, train_label, batchsize):
                    _, err, acc = sess.run([train_op, loss, accuracy],
                                           feed_dict={x: train_data_batch, y_: train_label_batch})
                    train_loss += err
                    train_acc += acc
                    batch_num += 1
                print("epoch:", i)
                print("train loss:", train_loss/batch_num)
                print("train acc:", train_acc/batch_num)
                saver.save(sess, os.path.join(MODEL_SAVE_PATH, MODEL_NAME),global_step=global_step)
    elif command=="test2":
        # test_data, test_label, train_data, train_label = load_data()
        x = tf.placeholder(tf.float32, [None, 28, 28, 1], name='x')
        y_ = tf.placeholder(tf.float32, [None, 10], name='y_')
        validate_feed = {x: test_data, y_: test_label}

        y = inference(x,False,None)
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        saver = tf.train.Saver()
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            ckpt = tf.train.get_checkpoint_state(r'Model/')
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                print("model load over")
            # ckpt = tf.train.get_checkpoint_state(MODEL_SAVE_PATH)
            # if ckpt and ckpt.model_checkpoint_path:
            #     sess.run(tf.global_variables_initializer())
            #     saver = tf.train.import_meta_graph(r'Model/')
            #     saver.restore(sess, ckpt.model_checkpoint_path)
                graph = tf.get_default_graph()
                # graph.get_tensor_by_name("accuracy:0")
                # global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                accuracy_score = sess.run(accuracy, feed_dict=validate_feed)

                print("validation accuracy = %g" % (accuracy_score))
            else:
                print('No checkpoint file found')
        return


if __name__ == "__main__":
    ted, tel, trd, trl = load_data()
    main(ted, tel, trd, trl, "test2")
