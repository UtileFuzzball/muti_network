import tensorflow as tf

IMAGE_CHANNEL=1
IMAGE_SIZE=28
INPUT_NODE=784
OUTPUT_NODE=10

CONV1_SIZE=5
CONV1_DEEP=6

POOL1_SIZE=2
POOL1_STRIDE=2
# POOL1_DEEP=6

CONV2_SIZE=5
CONV2_DEEP=16

POOL2_SIZE=2
POOL2_STRIDE=2

FC1_SIZE=120

FC2_SIZE=84


def inference(input_tensor,regularizer=None):
    with tf.variable_scope('L1'):
        conv1_weight=tf.get_variable("L1_CONV1_WEIGHT", [CONV1_SIZE,CONV1_SIZE,IMAGE_CHANNEL,CONV1_DEEP],initializer=tf.truncated_normal_initializer(stddev=0.1))
        conv1_biases=tf.get_variable("L1_CONV1_BIASES",[CONV1_DEEP],initializer=tf.constant_initializer(0.0))
        conv1=tf.nn.conv2d(input_tensor,conv1_weight,strides=[1,1,1,1],padding="VALID")
        pre_activate1=tf.add(conv1,conv1_biases,name="L1_CONV")
        relu1=tf.nn.relu(pre_activate1,name="L1_RELU")
        pass

    with tf.variable_scope('L2'):
        # pool_weight=tf.get_variable("pool_weight",shape=[1,POOL1_SIZE,POOL1_SIZE,1],)
        pool1=tf.nn.max_pool(relu1,ksize=[1,POOL1_SIZE,POOL1_SIZE,1],strides=[1,2,2,1],padding="VALID",name="L2_pool")
        pass

    with tf.variable_scope('L3'):
        conv2_weight=tf.get_variable("L3_CONV_WEIGHT",[CONV2_SIZE,CONV2_SIZE,CONV1_DEEP,CONV2_DEEP],initializer=tf.truncated_normal_initializer(stddev=0.1))
        conv2_biases=tf.get_variable("L3_CONV_BIASES",[CONV2_DEEP],initializer=tf.constant_initializer(0.0))
        conv2=tf.nn.conv2d(pool1,conv2_weight,strides=[1,1,1,1],padding="VALID")
        pre_activate2=tf.add(conv2,conv2_biases,name="L3_CONV")
        relu2=tf.nn.relu(pre_activate2,name="L3_RELU")
        pass
    """ pool 5*5*16 """
    with tf.variable_scope('L4'):
        pool2=tf.nn.max_pool(relu2,ksize=[1,POOL2_SIZE,POOL2_SIZE,1],strides=[1,2,2,1],padding="VALID",name="L4_pool")

    flatted1=tf.contrib.layers.flatten(pool2)
    flatted1_nodes=flatted1.get_shape().as_list()[1]

    with tf.variable_scope('L5'):
        fc1_weights = tf.get_variable("L5_WEIGHTS", [flatted1_nodes, FC1_SIZE],
                                      initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(fc1_weights))
        fc1_bias = tf.get_variable("L5_BIASES", [FC1_SIZE], initializer=tf.constant_initializer(0.1))
        with tf.name_scope("Weight_plus_bias"):
            pre_activate3 = tf.matmul(flatted1, fc1_weights) + fc1_bias
        fc1 = tf.nn.relu(pre_activate3)

    with tf.variable_scope('L6'):
        fc2_weights = tf.get_variable("L6_WEIGHTS", [FC1_SIZE, FC2_SIZE],
                                      initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(fc2_weights))
        fc2_bias = tf.get_variable("L6_BIASES", [FC2_SIZE], initializer=tf.constant_initializer(0.1))
        with tf.name_scope("Weight_plus_bias"):
            pre_activate4 = tf.add(tf.matmul( fc1,fc2_weights) , fc2_bias)
        fc2 = tf.nn.relu(pre_activate4)

    with tf.variable_scope("L7"):
        fc3_weights=tf.get_variable("L7_WEIGHTS",[FC2_SIZE,OUTPUT_NODE],initializer=tf.truncated_normal_initializer(stddev=0.1))
        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(fc3_weights))
        fc3_bias=tf.get_variable("L7_BIASES",[OUTPUT_NODE],initializer=tf.truncated_normal_initializer(stddev=0.1))
        with tf.name_scope("Weight_plus_bias"):
            fc3=tf.add(tf.matmul(fc2,fc3_weights),fc3_bias)
    return fc3