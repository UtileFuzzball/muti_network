import LeNet5_inference
import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
import os

LEARNING_RATE_DECAY=0.98
# CKPT_PATH="./ckpt/"
BATCH_SIZE=50
LEARNING_RATE_BASE=0.8
TRAINING_STEPS = 30000
MOVING_AVERAGE_DECAY = 0.99
MODEL_SAVE_PATH="model/"
MODEL_NAME="LeNet5_model"

def train(mnist):
    with tf.name_scope("input"):
        x = tf.placeholder(tf.float32,[None, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_CHANNEL],
                           name="x-input")
        y_label = tf.placeholder(tf.float32, [None, LeNet5_inference.OUTPUT_NODE], name="y-input")
        regularizer=tf.contrib.layers.l2_regularizer(0.001)
        y = LeNet5_inference.inference(x,regularizer)
        global_step=tf.Variable(0,trainable=False)
    with tf.name_scope("accuracy"):
        # correct_prediction = tf.cast(tf.equal(tf.argmax(y, 1), tf.argmax(y_label, 1)), tf.float32)
        # accuracy = tf.reduce_mean(correct_prediction)
        variable_averages = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, global_step)
        variables_averages_op = variable_averages.apply(tf.trainable_variables())

        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=tf.argmax(y_label,1), logits=y)
        cross_entropy_mean = tf.reduce_mean(cross_entropy)
        loss = cross_entropy_mean+ tf.add_n(tf.get_collection('losses')) #+tf.add_n(tf.get_collection('losses'))
        learning_rate = tf.train.exponential_decay(
            LEARNING_RATE_BASE,
            global_step,
            mnist.train.num_examples / BATCH_SIZE, LEARNING_RATE_DECAY,
            staircase=True)
        # train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)
        # with tf.control_dependencies([train_step, variables_averages_op]):
        #     train_op = tf.no_op(name='train')
        train_op = tf.train.AdamOptimizer(0.001).minimize(loss)
        print(cross_entropy)
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_label, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")

    saver = tf.train.Saver()

    with tf.Session() as sess:
        init_op = tf.global_variables_initializer()
        sess.run(init_op)  # init


        for i in range(TRAINING_STEPS):
            xs, ys = mnist.train.next_batch(BATCH_SIZE)
            xs_reshaped = np.reshape(xs, (BATCH_SIZE, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_SIZE, LeNet5_inference.IMAGE_CHANNEL))
            _, loss_value, step,acc = sess.run([train_op, loss, global_step,accuracy],feed_dict={x: xs_reshaped, y_label: ys})
            if i % 500 == 0:
                saver.save(sess, os.path.join(MODEL_SAVE_PATH, MODEL_NAME), global_step=global_step)
                print("After %d training step(s), loss on training batch is %g %f." % (step, loss_value,acc))
                # print()
def main(argv=None):
    mnist = input_data.read_data_sets("../minst/datasets/MNIST_data", one_hot=True)
    train(mnist)

if __name__ == '__main__':
    tf.app.run()