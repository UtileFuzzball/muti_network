import tensorflow as tf

# layer size
CONV1_SIZE = 5
CONV1_DEEP = 6

POOL1_SIZE = 2
POOL1_STRIDE = 2

CONV2_SIZE = 5
CONV2_DEEP = 16

POOL2_SIZE = 2
POOL2_STRIDE = 2

FC1_SIZE = 120

FC2_SIZE = 84

# image input
NUM_CHANNAL = 1  # 图片是32*32*1
IMAGE_SIZE = 28

INPUT_NODE = 784
NUM_LABEL = 10  # 也是以后一层全连接层的输出节点数

BATCH_SIZE = 50


# tensor 的均值和方差可视化
def variable_summaries(name, var):
    # 计算变量 var 的平均值、标准差。
    with tf.name_scope(name):
        tf.summary.histogram(name, var)  # 计算 var 张量中元素的取值分布
        mean = tf.reduce_mean(var)
        tf.summary.scalar(name + "/mean", mean)
        stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar(name + "/stddev", stddev)


# 前向通道
def LeNet_forward(INPUT_TENSOR):
    with tf.variable_scope("layer1_CONV1"):
        CONV1_Weights = tf.get_variable("Weight", [CONV1_SIZE, CONV1_SIZE, NUM_CHANNAL, CONV1_DEEP],
                                        initializer=tf.truncated_normal_initializer(stddev=0.1))
        variable_summaries("Weight", CONV1_Weights)
        CONV1_bias = tf.get_variable("bias", [CONV1_DEEP], initializer=tf.constant_initializer(0.1))
        variable_summaries("bias", CONV1_bias)
        CONV1 = tf.nn.conv2d(INPUT_TENSOR, CONV1_Weights, strides=[1, 1, 1, 1], padding='VALID')
        with tf.name_scope("Weight_plus_bias"):
            pre_activate = tf.nn.bias_add(CONV1, CONV1_bias)
            tf.summary.histogram("pre_activate", pre_activate)
        RELU1 = tf.nn.relu(pre_activate)
        tf.summary.histogram("relu_activate", RELU1)
        print(RELU1)

    with tf.name_scope("layer2_POOL1"):
        POOL1 = tf.nn.max_pool(RELU1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')
        tf.summary.histogram("relu_activate", POOL1)
        print(POOL1)

    with tf.variable_scope("layer3_CONV2"):
        CONV2_Weights = tf.get_variable("Weight", [CONV2_SIZE, CONV2_SIZE, CONV1_DEEP, CONV2_DEEP],
                                        initializer=tf.truncated_normal_initializer(stddev=0.1))
        variable_summaries("Weight", CONV2_Weights)
        CONV2_bias = tf.get_variable("bias", [CONV2_DEEP], initializer=tf.constant_initializer(0.1))
        variable_summaries("bias", CONV2_bias)
        CONV2 = tf.nn.conv2d(POOL1, CONV2_Weights, strides=[1, 1, 1, 1], padding='VALID')
        with tf.name_scope("Weight_plus_bias"):
            pre_activate = tf.nn.bias_add(CONV2, CONV2_bias)
            tf.summary.histogram("pre_activate", pre_activate)
        RELU2 = tf.nn.relu(pre_activate)
        tf.summary.histogram("relu_activate", RELU2)
        print(RELU2)

    with tf.name_scope("layer4_POOL2"):
        POOL2 = tf.nn.max_pool(RELU2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')
        tf.summary.histogram("relu_activate", POOL2)

    FLATTED1 = tf.contrib.layers.flatten(POOL2)
    FLATTED1_NODES = FLATTED1.get_shape().as_list()[1]
    tf.summary.histogram("FLATTED", FLATTED1)
    print(FLATTED1)

    with tf.variable_scope("layer5_FC1"):
        FC1_Weights = tf.get_variable("Weight", [FLATTED1_NODES, FC1_SIZE],
                                      initializer=tf.truncated_normal_initializer(stddev=0.1))
        variable_summaries("Weight", FC1_Weights)
        FC1_bias = tf.get_variable("bias", [FC1_SIZE], initializer=tf.constant_initializer(0.1))
        variable_summaries("bias", FC1_bias)
        with tf.name_scope("Weight_plus_bias"):
            pre_activate = tf.matmul(FLATTED1, FC1_Weights) + FC1_bias
            tf.summary.histogram("pre_activate", pre_activate)
        FC1 = tf.nn.relu(pre_activate)
        tf.summary.histogram("relu_activate", FC1)

    with tf.variable_scope("layer6_FC2"):
        FC2_Weights = tf.get_variable("Weight", [FC1_SIZE, FC2_SIZE],
                                      initializer=tf.truncated_normal_initializer(stddev=0.1))
        variable_summaries("Weight", FC2_Weights)
        FC2_bias = tf.get_variable("bias", [FC2_SIZE], initializer=tf.constant_initializer(0.1))
        variable_summaries("bias", FC1_bias)
        with tf.name_scope("Weight_plus_bias"):
            pre_activate = tf.matmul(FC1, FC2_Weights) + FC2_bias
            tf.summary.histogram("pre_activate", pre_activate)
        FC2 = tf.nn.relu(pre_activate)
        tf.summary.histogram("relu_activate", FC2)

    with tf.variable_scope("layer7_FC3"):
        FC3_Weights = tf.get_variable("Weight", [FC2_SIZE, NUM_LABEL],
                                      initializer=tf.truncated_normal_initializer(stddev=0.1))
        variable_summaries("Weight", FC3_Weights)
        FC3_bias = tf.get_variable("bias", [NUM_LABEL], initializer=tf.constant_initializer(0.1))
        variable_summaries("bias", FC3_bias)
        with tf.name_scope("Weight_plus_bias"):
            logits = tf.matmul(FC2, FC3_Weights) + FC3_bias
            tf.summary.histogram("logits", logits)
    return logits
