import LeNet_forward
import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data

MODEL_SAVE_PATH="./model/"
EVENT_SAVE_PATH="./event/"
BATCH_SIZE=50

def train(mnist):
    with tf.name_scope("input"):
        x = tf.placeholder(tf.float32,
                           [BATCH_SIZE, LeNet_forward.IMAGE_SIZE, LeNet_forward.IMAGE_SIZE, LeNet_forward.NUM_CHANNAL],
                           name="input-data")
        y = tf.placeholder(tf.float32, [BATCH_SIZE, LeNet_forward.NUM_LABEL], name="label")
    y_pred = LeNet_forward.LeNet_forward(x)

    with tf.name_scope("accuracy"):
        correct_prediction = tf.cast(tf.equal(tf.argmax(y_pred, 1), tf.argmax(y, 1)), tf.float32)
        accuracy = tf.reduce_mean(correct_prediction)
        tf.summary.scalar("accuracy", accuracy)

    with tf.name_scope("loss"):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=y_pred)
        cross_entropy_mean = tf.reduce_mean(cross_entropy)
        tf.summary.scalar("cross_entropy_mean", cross_entropy_mean)
        loss = cross_entropy_mean
        tf.summary.scalar("loss", loss)
        print(cross_entropy)

    with tf.name_scope("train_step"):
        LEARNING_RATE = 0.01
        train_step = tf.train.AdamOptimizer(LEARNING_RATE).minimize(cross_entropy)

    merged = tf.summary.merge_all()
    saver = tf.train.Saver(max_to_keep=3)
    global_step = tf.Variable(0, trainable=False)

    with tf.Session() as sess:
        writer = tf.summary.FileWriter(EVENT_SAVE_PATH, sess.graph)
        init_op = tf.global_variables_initializer()
        sess.run(init_op)  # 初始化
        TRAIN_STEPS = 10000

        for i in range(TRAIN_STEPS):
            xs, ys = mnist.train.next_batch(BATCH_SIZE)
            xs_reshaped = np.reshape(xs, (
            BATCH_SIZE, LeNet_forward.IMAGE_SIZE, LeNet_forward.IMAGE_SIZE, LeNet_forward.NUM_CHANNAL))
            summary, _, cross_entropy_value, loss_value, accuracy_value = sess.run(
                [merged, train_step, cross_entropy, loss, accuracy],
                feed_dict={x: xs_reshaped, y: ys})
            writer.add_summary(summary, i)
            if (i + 1) % 1000 == 0:
                saver.save(sess, MODEL_SAVE_PATH)
                print("after %d train steps, cross_entropy: %g. loss: %g. accuracy: %g." \
                      % (i + 1, cross_entropy_value[-1], loss_value, accuracy_value), "model saved!")

        writer.close()


def main(argv=None):
    mnist = input_data.read_data_sets("../mnist/datasets/MNIST_data", one_hot=True)
    train(mnist)


if __name__ == '__main__':
    tf.app.run()