import time
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import mnist_inference
import mnist_train
import numpy
from tensorflow.python.framework import graph_util

EVAL_INTERVAL_SECS = 10
PB_FILE_PATH = "./model_pb/"


def evaluate(mnist):
    with tf.Graph().as_default() as g:
        x = tf.placeholder(tf.float32, [None, mnist_inference.INPUT_NODE], name='x-input')
        y_ = tf.placeholder(tf.float32, [None, mnist_inference.OUTPUT_NODE], name='y-input')
        # validate_feed = {x: mnist.validation.images, y_: mnist.validation.labels}
        validate_feed = {x: mnist.test.images, y_: mnist.test.labels}
        y__ = y_
        y = mnist_inference.inference(x, None)

        yargmax = tf.argmax(y, 1, name="yargmax")
        y_argmax = tf.argmax(y_, 1)
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        # print(accuracy)
        variable_averages = tf.train.ExponentialMovingAverage(mnist_train.MOVING_AVERAGE_DECAY)
        variables_to_restore = variable_averages.variables_to_restore()
        saver = tf.train.Saver(variables_to_restore)

        # while True:
        with tf.Session() as sess:
            ckpt = tf.train.get_checkpoint_state(mnist_train.MODEL_SAVE_PATH)
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]

                """ M: get all tensor in default_graph() """
                tensor_name_list=[tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
                print(tensor_name_list)

                """print layer1"""
                graph = tf.get_default_graph()
                fc1 = graph.get_operation_by_name('layer1/Relu').outputs[0]
                print(fc1) #output: (?,500)
                result_fc1=sess.run(fc1,feed_dict=validate_feed)
                # print(len(result_fc1[0]))
                # print(numpy.shape(result_fc1))
                # print(result_fc1[0])
                for j in range(len(result_fc1)):
                    with open("./data/layer1_relu_mlu/fc1_" + str(j) + ".txt", "w")as file:
                    #with open("./data/layer1_relu_cpu/fc1_"+str(j)+".txt","w")as file:
                        for i in result_fc1[j]:
                            file.write("%f\n"%i)
                    # with open("/home/wangsiqi/Desktop/fc1_9999.txt","w")as file:
                    #     for i in result_fc1[9999]:
                    #         file.write("%f\n"%i)

                """print layer2"""
                graph = tf.get_default_graph()
                fc2 = graph.get_operation_by_name('layer2/add').outputs[0]
                print(fc2) #output: (?,500)
                result_fc2=sess.run(fc2,feed_dict=validate_feed)
                # print(len(result_fc2[0]))
                print(numpy.shape(result_fc2))
                # print(result_fc1[0])
                # with open("/home/wangsiqi/Desktop/fc2_0.txt","w")as file:
                    # for i in result_fc2[0]:
                    #     file.write("%d\n"%i)
                with open("./data/layer2_mlu.txt","w")as file:
                    for i in result_fc2:
                        for j in i:
                            file.write("%f "%j)
                        file.write("\n")

                accuracy_score = sess.run(accuracy, feed_dict=validate_feed)
                print("After %s training step(s), validation accuracy = %g" % (global_step, accuracy_score))


            else:
                print('No checkpoint file found')
                return

        time.sleep(EVAL_INTERVAL_SECS)


def main(argv=None):
    mnist = input_data.read_data_sets("./datasets/MNIST_data", one_hot=True)
    evaluate(mnist)


if __name__ == '__main__':
    main()

