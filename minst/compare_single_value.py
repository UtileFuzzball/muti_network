import tensorflow as tf
import numpy as np

wrong_list=[]
average=[]
with open("single_wrong_name.txt")as file:
    lines=file.readlines()
    for line in lines:
        tt=line.split()
        wrong_list.append(tt[0])


for i in wrong_list:
    cpu_data=[]
    mlu_data=[]
    difference=[]
    # print("/wrong list/cpu_fc1_"+i+".txt")
    with open("./wrong list/cpu_fc1_"+i+".txt") as file:
        lines=file.readlines()
        for line in lines:
            cpu_data.append(float(line))
    with open("./wrong list/mlu_fc1_"+i+".txt") as file:
        lines=file.readlines()
        for line in lines:
            mlu_data.append(float(line))

    for j in range(len(cpu_data)):
        difference.append(cpu_data[j]-mlu_data[j])
    print(i)
    print(len(difference))
    average.append(np.mean(difference))
print(average)

with open("layer1_difference.txt","w")as file:
    for i in average:
        file.write(str(i)+"\n")
