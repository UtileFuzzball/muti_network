import os

import tensorflow as tf
import numpy as np
from alexnet import AlexNet
import matplotlib.pyplot as plt
def get_data():
    """ Get Data """

    # File Path
    CIFAR_DIR = 'Data/cifar-10-batches-py/'

    # Load the Data
    def unpickle(file):
        import pickle
        with open(file, 'rb') as fo:
            cifar_dict = pickle.load(fo, encoding='bytes')
        return cifar_dict

    dirs = ['batches.meta', 'data_batch_1', 'data_batch_2', 'data_batch_3', 'data_batch_4', 'data_batch_5',
            'test_batch']
    all_data = [0, 1, 2, 3, 4, 5, 6]

    for i, direc in zip(all_data, dirs):
        all_data[i] = unpickle(CIFAR_DIR + direc)

    batch_meta = all_data[0]
    data_batch1 = all_data[1]
    data_batch2 = all_data[2]
    data_batch3 = all_data[3]
    data_batch4 = all_data[4]
    data_batch5 = all_data[5]
    test_batch = all_data[6]
    return all_data

def one_hot_encode(vec, vals=10):
    '''
    For use to one-hot encode the 10- possible labels
    '''
    n = len(vec)
    out = np.zeros((n, vals))
    out[range(n), vec] = 1
    return out

class CifarHelper():

    def __init__(self):
        self.i = 0

        # Grabs a list of all the data batches for training
        self.all_train_batches = [data_batch1, data_batch2, data_batch3, data_batch4, data_batch5]
        # Grabs a list of all the test batches (really just one batch)
        self.test_batch = [test_batch]

        # Intialize some empty variables for later on
        self.training_images = None
        self.training_labels = None

        self.test_images = None
        self.test_labels = None

    def set_up_images(self):
        print("Setting Up Training Images and Labels")

        # Vertically stacks the training images
        self.training_images = np.vstack([d[b"data"] for d in self.all_train_batches])
        train_len = len(self.training_images)

        # Reshapes and normalizes training images
        self.training_images = self.training_images.reshape(train_len, 3, 32, 32).transpose(0, 2, 3, 1) / 255
        # One hot Encodes the training labels (e.g. [0,0,0,1,0,0,0,0,0,0])
        self.training_labels = one_hot_encode(np.hstack([d[b"labels"] for d in self.all_train_batches]), 10)

        print("Setting Up Test Images and Labels")

        # Vertically stacks the test images
        self.test_images = np.vstack([d[b"data"] for d in self.test_batch])
        test_len = len(self.test_images)

        # Reshapes and normalizes test images
        self.test_images = self.test_images.reshape(test_len, 3, 32, 32).transpose(0, 2, 3, 1) / 255
        # One hot Encodes the test labels (e.g. [0,0,0,1,0,0,0,0,0,0])
        self.test_labels = one_hot_encode(np.hstack([d[b"labels"] for d in self.test_batch]), 10)

    def next_batch(self, batch_size):
        # Note that the 100 dimension in the reshape call is set by an assumed batch size of 100
        x = self.training_images[self.i:self.i + batch_size].reshape(100, 32, 32, 3)
        y = self.training_labels[self.i:self.i + batch_size]
        self.i = (self.i + batch_size) % len(self.training_images)
        return x, y

def train():
    BATCH=100
    steps=10001

    # placeholder for input and dropout rate
    x = tf.placeholder(tf.float32, shape=[None, 32, 32, 3])
    y_true = tf.placeholder(tf.float32, shape=[None, 10])
    keep_prob = tf.placeholder(tf.float32)

    # Create the AlexNet model
    model = AlexNet(x=x, keep_prob=keep_prob, num_classes=10)

    # define activation of last layer as score
    score = model.fc8
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_true, logits=score))
    optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
    train = optimizer.minimize(cross_entropy)

    saver=tf.train.Saver()
    # steps = 10,000 will create 20 epochs.
    # There are a total of 50,000 images in the training set.
    # (10,000 * 100) / 50,000 = 20
    # steps = 10001

    ch = CifarHelper()
    # pre-processes the data.
    ch.set_up_images()

    with tf.Session() as sess:
        init=tf.global_variables_initializer()
        sess.run(init)

        for i in range(steps):

            # get next batch of data.
            batch = ch.next_batch(100)
            # On training set.
            sess.run(train, feed_dict={x: batch[0], y_true: batch[1], keep_prob: 0.5})

            # Print accuracy after every epoch.
            # 500 * 100 = 50,000 which is one complete batch of data.
            if i % 500 == 0:
                print("EPOCH: {}".format(i / 500))
                print("ACCURACY ")

                matches = tf.equal(tf.argmax(score, 1), tf.argmax(y_true, 1))
                acc = tf.reduce_mean(tf.cast(matches, tf.float32))

                # On valid/test set.
                print(sess.run(acc, feed_dict={x: ch.test_images, y_true: ch.test_labels, keep_prob: 1.0}))
                print('\n')
            saver.save(sess,os.path.join(MODEL_SAVE_PATH,MODEL_NAME))

if __name__ == '__main__':
    MODEL_SAVE_PATH="./model/"
    MODEL_NAME="alexnet_model.ckpt"

    all_data=get_data()
    batch_meta = all_data[0]
    data_batch1 = all_data[1]
    data_batch2 = all_data[2]
    data_batch3 = all_data[3]
    data_batch4 = all_data[4]
    data_batch5 = all_data[5]
    test_batch = all_data[6]
    train()