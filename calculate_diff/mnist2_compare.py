import os
import pandas as pd
MLU_PATH="/home/hchsmart/mlu100/v7.3.2/Cambricon-Test/tensorflow/cambricon_examples/script/muti_network/mnist3/mlu_data/"
CPU_PATH="/home/wangsiqi/w77/PycharmProjects/mnist3/cpu_data/"
DIF_PATH="/home/wangsiqi/w77/PycharmProjects/mnist dif3/"

L1_MATMUL_PATH="l1_matmul/"
L1_ADD_PATH="l1_add/"
L1_RELU_PATH="l1_relu/"
L2_MATMUL_PATH="l2_matmul/"
L2_ADD_PATH="l2_add/"
L2_SOFTMAX_PATH="l2_softmax/"

def get_file_num(check_path):
    num=0
    ori_path=os.getcwd()
    files=os.listdir(check_path)
    for e in files:
        if os.path.isfile(os.path.join(check_path,e)):
            num+=1
    # print('filenum:',len([lists for lists in os.listdir(path) if os.path.isfile(os.path.join(path, lists))]))
    return num

def cal_result(save_path):
    result_cpu=[]
    result_mlu=[]
    dif=[]
    for i in range(len(result_cpu)):
        dif.append(result_cpu[i]-result_mlu[i])
    with open(save_path,"w")as file:
        for i in range(len(dif)):
            file.write("%f"%dif[i])

# cpu_l1_biases=pd.read_csv(CPU_PATH+"cpu_l1_biases.txt",header=None)
# cpu_l1_weights=pd.read_csv(CPU_PATH+"cpu_l1_weights.txt",header=None)
# cpu_l2_biases=pd.read_csv(CPU_PATH+"cpu_l2_biases.txt",header=None)
# cpu_l2_weights=pd.read_csv(CPU_PATH+"cpu_l2_weights.txt",header=None)

def read_path(type,file_name=None):
    if type is not "":
        file_name1=type.split("/")[0]
        print(file_name1)
        size=get_file_num(CPU_PATH+type)
        print(size)
        for i in range(size):
            cpu_path=CPU_PATH+type+"cpu_"+file_name1+"_"+str(i)+".txt"
            mlu_path=MLU_PATH+type+"mlu_"+file_name1+"_"+str(i)+".txt"
            dif_path=DIF_PATH+type+"dif_"+file_name1+"_"+str(i)+".txt"
            cpu_result_list=pd.read_csv(cpu_path, header=None)
            mlu_result_list=pd.read_csv(mlu_path,header=None)
            dif=[]
            for j in range(len(cpu_result_list)):
                dif.append(cpu_result_list[0][j]-mlu_result_list[0][j])
            df=pd.DataFrame(dif)
            # print(df)
            df.to_csv(dif_path,index=False,header=False,float_format='%.6f')
            # return
    else:
        cpu_path = CPU_PATH + type + "cpu_" + file_name + ".txt"
        mlu_path = MLU_PATH + type + "mlu_" + file_name + ".txt"
        dif_path = DIF_PATH + type + "dif_" + file_name + ".txt"
        cpu_result_list = pd.read_csv(cpu_path, header=None)
        mlu_result_list = pd.read_csv(mlu_path, header=None)
        dif = []
        # print(cpu_result_list)
        # print(str(type(mlu_result_list)))
        for j in range(len(cpu_result_list)):
            dif.append(cpu_result_list[0][j] - mlu_result_list[0][j])
        df = pd.DataFrame(dif)
        df.to_csv(dif_path, index=False)
        print(df)
        return

def printResult():
    # MLU_PATH = "/home/hchsmart/mlu100/v7.3.2/Cambricon-Test/tensorflow/cambricon_examples/script/muti_network/mnist2/mlu_data/"
    # CPU_PATH = "/home/wangsiqi/w77/PycharmProjects/muti_network/mnist2/cpu_data/"
    # DIF_PATH = "/home/wangsiqi/w77/PycharmProjects/dif/"
    label_result=pd.read_csv(CPU_PATH+"cpu_label_argmax.txt",header=None)
    cpu_result=pd.read_csv(CPU_PATH+"cpu_y_argmax.txt",header=None)
    mlu_result=pd.read_csv(MLU_PATH+"mlu_y_argmax.txt",header=None)

    # for i in range(len(cpu_result)):
    #     ll.append(label_result[0])
    label_result.set_axis(['label_argmax'],axis=1)
    cpu_result.set_axis(['cpu_argmax'],axis=1)
    mlu_result.set_axis(['mlu_argmax'],axis=1)
    ll=label_result.join(cpu_result,how="right")
    tt=ll.join(mlu_result,how="right")
    tt.to_csv(DIF_PATH+"argmax.txt")

def test_for_check_path():
    print(CPU_PATH+L1_ADD_PATH)
    print(get_file_num(CPU_PATH+L1_ADD_PATH))

if __name__ == '__main__':
    read_path(L1_ADD_PATH)
    read_path(L1_MATMUL_PATH)
    read_path(L1_RELU_PATH)
    read_path(L2_ADD_PATH)
    read_path(L2_MATMUL_PATH)
    # read_path(L2_SOFTMAX_PATH)
    printResult()