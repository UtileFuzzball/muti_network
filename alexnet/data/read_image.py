import numpy as np
import os


train_label = {}

for i in range(10):
    search_path = './data/train/{}'.format(i)
    file_list = os.listdir(search_path)
    for file in file_list:
        train_label[os.path.join(search_path, file)] = i

np.save('label.npy', train_label)

test_label = {}

for i in range(10):
    search_path = './data/test/{}'.format(i)
    file_list = os.listdir(search_path)
    for file in file_list:
        test_label[os.path.join(search_path, file)] = i

np.save('test-label.npy', test_label)