# from __future__ import print_function
import tensorflow as tf
from datetime import datetime
import math,time
import tensorflow as tf
import pickle

from alexnet_inference import inference

mlu_model_path="/home/hchsmart/mlu100/v7.3.2/models-datasets/models/tensorflow_models/tf_inference/alexnet"
batch_size = 32
num_bathes = 100

def resore_graph():
    with tf.Session() as sess:
        saver=tf.train.import_meta_graph(mlu_model_path+"/checkpoint-200.meta")
        saver.restore(sess,"./checkpoint.ckpt")
        # print sess.run()
        list=[n.name for n in tf.get_default_graph().as_graph_def().node]
        print(list)


'''
count time of one training
session:TensorFlow的Session
target:op need to calculate
info_string:test name 
'''
def time_tensorflow_run(session,target,info_string):

    num_step_burn_in = 10
    total_duration = 0.0
    total_duration_squared = 0.0
    for i in range(num_bathes + num_step_burn_in):
        start_time = time.time()
        _ = session.run(target)
        duration = time.time() - start_time
        if i >= num_step_burn_in:
            if not i % 10 :
                print("%s:step %d,duration=%.3f"%(datetime.now(),i-num_step_burn_in,duration))
            total_duration += duration
            total_duration_squared += duration * duration

    mn = total_duration / num_bathes

    vr = total_duration_squared / num_bathes - mn * mn
    std = math.sqrt(vr)
    print("%s:%s across %d steps,%.3f +/- %.3f sec / batch"%(datetime.now(),info_string,num_bathes,
                                                             mn,std))
#main function
def run_bechmark():
    with tf.Graph().as_default():
        image_size = 224

        images = tf.Variable(tf.random_normal([batch_size,image_size,image_size,3],
                                              dtype=tf.float32,stddev=0.1))
        output,parameters = inference(images)
        init = tf.global_variables_initializer()
        sess = tf.Session()
        sess.run(init)
        time_tensorflow_run(sess,output,"Forward")
        objective = tf.nn.l2_loss(output)
        grad = tf.gradients(objective,parameters)
        time_tensorflow_run(sess,grad,"Forward-backward")
def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        cifar_dict = pickle.load(fo, encoding='bytes')
    return cifar_dict

def train():
    CIFAR_DIR="../cifar-10-batches-py/"
    dirs=['batches.meta','data_batch_1','data_batch_2','data_batch_3','data_batch_4','data_batch_5','test_batch']
    all_data=[i for i in range(0,7,1)]
    for i in range(0,7,1):
        all_data[i]=unpickle(CIFAR_DIR+dirs[i])
    batch_meta = all_data[0]
    data_batch1 = all_data[1]
    data_batch2 = all_data[2]
    data_batch3 = all_data[3]
    data_batch4 = all_data[4]
    data_batch5 = all_data[5]
    test_batch = all_data[6]

    with tf.Graph().as_default():
        # batch_size
        pass
    pass


if __name__ == "__main__":
    train()
    # run_bechmark()