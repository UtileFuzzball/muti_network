import tensorflow as tf

INPUT_NODE = 784
OUTPUT_NODE = 10
LAYER1_NODE = 500

def get_weight_variable(shape, regularizer):
    weights = tf.get_variable("weights", shape, initializer=tf.truncated_normal_initializer(stddev=0.1))
    if regularizer != None: tf.add_to_collection('losses', regularizer(weights))
    return weights


def inference(input_tensor, regularizer):
    with tf.variable_scope('layer1'):

        l1_weights = get_weight_variable([INPUT_NODE, LAYER1_NODE], regularizer)
        l1_biases = tf.get_variable(name="l1_biases", shape=[LAYER1_NODE], initializer=tf.constant_initializer(0.0))
        l1_matmul=tf.matmul(input_tensor,l1_weights,name="l1_matmul")
        l1_add=tf.add(l1_matmul,l1_biases,name="l1_add")
        layer1=tf.nn.relu(l1_add,name="l1_relu")
        # layer1 = tf.nn.relu(tf.matmul(input_tensor, weights) + biases)
        # tensor_name_list=[tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
        # print(tensor_name_list)
    with tf.variable_scope('layer2'):
        l2_weights = get_weight_variable([LAYER1_NODE, OUTPUT_NODE], regularizer)
        l2_biases = tf.get_variable(name="l2_biases", shape=[OUTPUT_NODE], initializer=tf.constant_initializer(0.0))
        l2_matmul=tf.matmul(layer1,l2_weights,name="l2_matmul")
        l2_add=tf.add(l2_matmul,l2_biases,name="l2_add")
        layer2 =tf.nn.softmax(l2_add,name="l2_softmax")
        # tensor_name_list=[tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
        # print(tensor_name_list)
        # tensor_name_list=[tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
        # print(tensor_name_list)
    return layer2

