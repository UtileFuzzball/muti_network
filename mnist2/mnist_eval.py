import time
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import mnist_inference
import mnist_train
import numpy
from tensorflow.python.framework import graph_util

EVAL_INTERVAL_SECS = 10
PB_FILE_PATH = "./model_pb/"
BATCH_SIZE = 10000
IF_CPU_MLU = "cpu_"
DATA_PATH=IF_CPU_MLU+"data/"

L1_MATMUL_PATH="l1_matmul/"
L1_ADD_PATH="l1_add/"
L1_RELU_PATH="l1_relu/"

L2_MATMUL_PATH="l2_matmul/"
L2_ADD_PATH="l2_add/"
L2_SOFTMAX_PATH="l2_softmax/"

def evaluate(mnist):
    with tf.Graph().as_default() as g:
        """ x and label y """
        x = tf.placeholder(tf.float32, [None, mnist_inference.INPUT_NODE], name='x-input')
        label = tf.placeholder(tf.float32, [None, mnist_inference.OUTPUT_NODE], name='label-input')
        # validate_feed = {x: mnist.validation.images, y_: mnist.validation.labels}
        """ get batch test """
        test_data, test_label = mnist.test.images,mnist.test.labels
        validate_feed = {x: test_data, label: test_label}
        y = mnist_inference.inference(x, None)

        y_argmax = tf.argmax(y, 1, name="y_argmax")
        label_argmax = tf.argmax(label, 1, name="label_argmax")
        correct_prediction = tf.equal(y_argmax, label_argmax)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        # print(accuracy)
        variable_averages = tf.train.ExponentialMovingAverage(mnist_train.MOVING_AVERAGE_DECAY)
        variables_to_restore = variable_averages.variables_to_restore()
        saver = tf.train.Saver(variables_to_restore)

        # while True:
        with tf.Session() as sess:
            ckpt = tf.train.get_checkpoint_state(mnist_train.MODEL_SAVE_PATH)
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]

                """ M: get all tensor in default_graph() """
                tensor_name_list = [tensor.name for tensor in tf.get_default_graph().as_graph_def().node]
                print(tensor_name_list)
                graph = tf.get_default_graph()

                """layer1"""

                """get l1-weights"""
                l1_weights = graph.get_operation_by_name('layer1/weights').outputs[0]
                print("l1_weights :" + str(l1_weights.shape))  # output: (?,500)

                """get l1-biases"""
                l1_biases = graph.get_operation_by_name('layer1/l1_biases').outputs[0]
                print("l1_biases :" + str(l1_biases.shape))  # output: (?,500)

                """get l1-matmul"""
                l1_matmul = graph.get_operation_by_name('layer1/l1_matmul').outputs[0]
                print("l1_matmul :" + str(l1_matmul.shape))  # output: (?,500)

                """get l1-add"""
                l1_add = graph.get_operation_by_name('layer1/l1_add').outputs[0]
                print("l1_add :" + str(l1_add.shape))  # output: (?,500)

                """get l1-relu"""
                l1_relu = graph.get_operation_by_name('layer1/l1_relu').outputs[0]
                print("l1_relu :" + str(l1_relu.shape))  # output: (?,500)

                """layer2"""
                """get l2-weights"""
                l2_weights = graph.get_operation_by_name('layer2/weights').outputs[0]
                print("l2_weights :" + str(l2_weights.shape))  # output: (?,500)

                """get l2-biases"""
                l2_biases = graph.get_operation_by_name('layer2/l2_biases').outputs[0]
                print("l2_biases :" + str(l2_biases.shape))  # output: (?,500)

                """get l2-matmul"""
                l2_matmul = graph.get_operation_by_name('layer2/l2_matmul').outputs[0]
                print("l2_matmul :" + str(l2_matmul.shape))  # output: (?,500)

                """get l2-add"""
                l2_add = graph.get_operation_by_name('layer2/l2_add').outputs[0]
                print("l2_add :" + str(l2_add.shape))  # output: (?,500)

                """get l2-softmax"""
                l2_softmax = graph.get_operation_by_name('layer2/l2_softmax').outputs[0]
                print("l2_softmax :" + str(l2_softmax.shape))  # output: (?,500)

                """ other """

                """ sess run all """
                l1_weights_op, l1_biases_op, l1_matmul_op, l1_add_op, l1_relu_op, \
                l2_weights_op, l2_biases_op, l2_matmul_op, l2_add_op, l2_softmax_op, \
                y_op, y_argmax_op, label_argmax_op, accuracy_op \
                    = sess.run([l1_weights, l1_biases, l1_matmul, l1_add, l1_relu,
                                l2_weights, l2_biases, l2_matmul, l2_add, l2_softmax,
                                y, y_argmax, label_argmax, accuracy], feed_dict=validate_feed)

                """print y"""
                """print weight and biases"""
                with open(DATA_PATH+IF_CPU_MLU+"l1_weights.txt", "w")as file:
                    for i in range(len(l1_weights_op)):
                        for j in range(len(l1_weights_op[i])):
                            file.write("%f\n" % l1_weights_op[i][j])

                with open(DATA_PATH+IF_CPU_MLU+"l1_biases.txt","w")as file:
                    for i in range(len(l1_biases_op)):
                        file.write("%f\n"%l1_biases_op[i])

                with open(DATA_PATH+IF_CPU_MLU+"l2_weights.txt", "w")as file:
                    for i in range(len(l2_weights_op)):
                        for j in range(len(l2_weights_op[i])):
                            file.write("%f\n" % l2_weights_op[i][j])

                with open(DATA_PATH+IF_CPU_MLU+"l2_biases.txt","w")as file:
                    for i in range(len(l2_biases_op)):
                        file.write("%f\n"%l2_biases_op[i])

                for i in range(len(l1_matmul_op)):
                    with open(DATA_PATH+L1_MATMUL_PATH+IF_CPU_MLU+"l1_matmul_"+str(i)+".txt","w")as file:
                        for j in range(len(l1_matmul_op[i])):
                            file.write("%f\n"%l1_matmul_op[i][j])

                for i in range(len(l1_add_op)):
                    with open(DATA_PATH+L1_ADD_PATH+IF_CPU_MLU+"l1_add_"+str(i)+".txt","w")as file:
                        for j in range(len(l1_add_op[i])):
                            file.write("%f\n"%l1_add_op[i][j])

                for i in range(len(l1_relu_op)):
                    with open(DATA_PATH+L1_RELU_PATH+IF_CPU_MLU+"l1_relu_"+str(i)+".txt","w")as file:
                        for j in range(len(l1_relu_op[i])):
                            file.write("%f\n"%l1_relu_op[i][j])

                for i in range(len(l2_matmul_op)):
                    with open(DATA_PATH+L2_MATMUL_PATH+IF_CPU_MLU+"l2_matmul_"+str(i)+".txt","w")as file:
                        for j in range(len(l2_matmul_op[i])):
                            file.write("%f\n"%l2_matmul_op[i][j])

                for i in range(len(l2_add_op)):
                    with open(DATA_PATH+L2_ADD_PATH+IF_CPU_MLU+"l2_add_"+str(i)+".txt","w")as file:
                        for j in range(len(l2_add_op[i])):
                            file.write("%f\n"%l2_add_op[i][j])

                for i in range(len(l2_softmax_op)):
                    with open(DATA_PATH+L2_SOFTMAX_PATH+IF_CPU_MLU+"l2_softmax_"+str(i)+".txt","w")as file:
                        for j in range(len(l2_softmax_op[i])):
                            file.write("%f\n"%l2_softmax_op[i][j])

                with open(DATA_PATH+IF_CPU_MLU+"y_argmax.txt","w")as file:
                    for i in range(len(y_argmax_op)):
                        file.write("%d\n"%y_argmax_op[i])

                with open(DATA_PATH+IF_CPU_MLU+"label_argmax.txt","w")as file:
                    for i in range(len(label_argmax_op)):
                        file.write("%d\n"%label_argmax_op[i])

                # for j in range(len())
                # graph = tf.get_default_graph()
                # fc1 = graph.get_operation_by_name('layer1/Relu').outputs[0]
                # print(fc1) #output: (?,500)
                # result_fc1=sess.run(fc1,feed_dict=validate_feed)
                # print(len(result_fc1[0]))
                # print(numpy.shape(result_fc1))
                # print(result_fc1[0])
                # for j in range(len(result_fc1)):
                #     with open("./data/layer1_relu_mlu/fc1_" + str(j) + ".txt", "w")as file:
                #     #with open("./data/layer1_relu_cpu/fc1_"+str(j)+".txt","w")as file:
                #         for i in result_fc1[j]:
                #             file.write("%f\n"%i)
                #     # with open("/home/wangsiqi/Desktop/fc1_9999.txt","w")as file:
                #     #     for i in result_fc1[9999]:
                #     #         file.write("%f\n"%i)

                """print layer2"""
                # graph = tf.get_default_graph()
                # fc2 = graph.get_operation_by_name('layer2/add').outputs[0]
                # print(fc2) #output: (?,500)
                # result_fc2=sess.run(fc2,feed_dict=validate_feed)
                # # print(len(result_fc2[0]))
                # print(numpy.shape(result_fc2))
                # # print(result_fc1[0])
                # # with open("/home/wangsiqi/Desktop/fc2_0.txt","w")as file:
                #     # for i in result_fc2[0]:
                #     #     file.write("%d\n"%i)
                # with open("./data/layer2_mlu.txt","w")as file:
                #     for i in result_fc2:
                #         for j in i:
                #             file.write("%f "%j)
                #         file.write("\n")

                # accuracy_score = sess.run(accuracy, feed_dict=validate_feed)
                print("After %s training step(s), validation accuracy = %f" % (global_step, accuracy_op))


            else:
                print('No checkpoint file found')
                return


def main(argv=None):
    mnist = input_data.read_data_sets("./datasets/MNIST_data", one_hot=True)
    evaluate(mnist)


if __name__ == '__main__':
    main()
